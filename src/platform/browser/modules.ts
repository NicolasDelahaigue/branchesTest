/**
 * Created by djavrell on 16/08/16.
 */

import {
  RouterModule
} from '@angular/router';

import {
  HttpModule
} from '@angular/http';

import { TranslateModule } from 'ng2-translate/ng2-translate';

const ANGULAR_MODULES= [
  RouterModule,
  HttpModule
];

const TRANSLATE_MODULE = [
  TranslateModule.forRoot()
];


export const MODULES = [
  TRANSLATE_MODULE,
];
